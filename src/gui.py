from dearpygui import dearpygui as dpg
from trainerbase.common import Vector3
from trainerbase.gui import (
    add_codeinjection_to_gui,
    add_gameobject_to_gui,
    add_script_to_gui,
    add_teleport_to_gui,
    simple_trainerbase_menu,
)

from injections import inf_ammo_elexit, inf_items, inf_jetpack_energy, no_reload
from objects import (
    attribute_point,
    cold,
    constitution,
    cunning,
    current_xp,
    dexterity,
    elexit,
    energy,
    health,
    intelligence,
    learning_points,
    max_energy,
    max_health,
    max_stamina,
    special,
    stamina,
    strength,
)
from scripts import energy_regeneration, health_regeneration
from teleport import tp


DPG_TAG_DASH_COEFFICIENT = "dpg_tag_dash_coefficient"


@simple_trainerbase_menu("ELEX", 800, 600)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_gameobject_to_gui(max_health, "Max Health")
            add_gameobject_to_gui(health, "Health", "F1")
            add_gameobject_to_gui(max_energy, "Max Energy")
            add_gameobject_to_gui(energy, "Energy", "F2")
            add_gameobject_to_gui(max_stamina, "Max Stamina")
            add_gameobject_to_gui(stamina, "Stamina", "F3")
            add_gameobject_to_gui(special, "Special", default_setter_input_value=100)
            add_gameobject_to_gui(elexit, "Elexit")

            add_gameobject_to_gui(strength, "Strength")
            add_gameobject_to_gui(constitution, "Constitution")
            add_gameobject_to_gui(dexterity, "Dexterity")
            add_gameobject_to_gui(intelligence, "Intelligence")
            add_gameobject_to_gui(cunning, "Cunning")
            add_gameobject_to_gui(current_xp, "Current Xp")
            add_gameobject_to_gui(cold, "Cold")
            add_gameobject_to_gui(learning_points, "Learning Points")
            add_gameobject_to_gui(attribute_point, "Attribute Point")

        with dpg.tab(label="Misc"):
            add_codeinjection_to_gui(inf_jetpack_energy, "Infinite Jetpack Energy", "F4")
            add_script_to_gui(health_regeneration, "Health Regeneration", "F6")
            add_script_to_gui(energy_regeneration, "Energy Regeneration", "F7")
            add_codeinjection_to_gui(inf_items, "Infinite Items", "F8")
            add_codeinjection_to_gui(inf_ammo_elexit, "Infinite Ammo/Elexit", "F10")
            add_codeinjection_to_gui(no_reload, "No Reload", "F11")

        with dpg.tab(label="Teleport"):
            add_teleport_to_gui(tp, "Insert", "Home", "End")
            dpg.add_slider_int(
                label="Dash Coefficient",
                tag=DPG_TAG_DASH_COEFFICIENT,
                min_value=5,
                max_value=200,
                default_value=5,
                callback=update_teleport_dash_coefficients,
            )


def update_teleport_dash_coefficients():
    dash_coefficient = dpg.get_value(DPG_TAG_DASH_COEFFICIENT)
    tp.dash_coefficients = Vector3(dash_coefficient, dash_coefficient, dash_coefficient)
