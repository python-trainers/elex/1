from trainerbase.codeinjection import CodeInjection
from trainerbase.memory import pm


inf_jetpack_energy = CodeInjection(pm.base_address + 0x768139, "nop\n" * 8)
inf_items = CodeInjection(pm.base_address + 0x58F67B, "nop\n" * 2)
inf_ammo_elexit = CodeInjection(pm.base_address + 0x566170, "nop\n" * 5)
no_reload = CodeInjection(pm.base_address + 0x6A8186, "nop\n" * 5)
