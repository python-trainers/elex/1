from trainerbase.gameobject import GameDouble, GameFloat, GameInt
from trainerbase.memory import Address, pm


base_address = Address(pm.base_address + 0xDE3718, [0x0, 0x28])

player_base_address = base_address.inherit(extra_offsets=[0x3E8])

# Stats
max_health = GameInt(player_base_address.inherit(extra_offsets=[0x234]))
health = GameInt(player_base_address.inherit(extra_offsets=[0x238]))
max_energy = GameInt(player_base_address.inherit(extra_offsets=[0x23C]))
energy = GameInt(player_base_address.inherit(extra_offsets=[0x240]))
max_stamina = GameInt(player_base_address.inherit(extra_offsets=[0x244]))
stamina = GameFloat(player_base_address.inherit(extra_offsets=[0x740, 0x288]))
special = GameFloat(player_base_address.inherit(extra_offsets=[0x740, 0x2D0]))
elexit = GameInt(player_base_address.inherit(extra_offsets=[0x10, 0x38, 0x10, 0x58, 0x15EC]))

# Attributes
strength = GameInt(player_base_address.inherit(extra_offsets=[0x24]))
constitution = GameInt(player_base_address.inherit(extra_offsets=[0x28]))
dexterity = GameInt(player_base_address.inherit(extra_offsets=[0x2C]))
intelligence = GameInt(player_base_address.inherit(extra_offsets=[0x30]))
cunning = GameInt(player_base_address.inherit(extra_offsets=[0x34]))
current_xp = GameInt(player_base_address.inherit(extra_offsets=[0x38]))
cold = GameInt(player_base_address.inherit(extra_offsets=[0x3C]))
learning_points = GameInt(player_base_address.inherit(extra_offsets=[0x40]))
attribute_point = GameInt(player_base_address.inherit(extra_offsets=[0x44]))

# Coords

player_coords_base_address = base_address.inherit(extra_offsets=[0x400, 0x218, 0x0, 0x8])

player_x = GameDouble(player_coords_base_address.inherit(extra_offsets=[0x1D0]))
player_z = GameDouble(player_coords_base_address.inherit(extra_offsets=[0x1D8]))
player_y = GameDouble(player_coords_base_address.inherit(extra_offsets=[0x1E0]))
