from trainerbase.common import regenerate
from trainerbase.scriptengine import ScriptEngine, system_script_engine

from objects import energy, health, max_energy, max_health
from teleport import tp


regeneration_script_engine = ScriptEngine(delay=0.3)


@regeneration_script_engine.simple_script
def health_regeneration():
    regenerate(health, max_health, percent=1, min_value=1)


@regeneration_script_engine.simple_script
def energy_regeneration():
    regenerate(energy, max_energy, percent=2, min_value=1)


system_script_engine.register_script(tp.create_movement_vector_updater_script())
