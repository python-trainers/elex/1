from trainerbase.common import Teleport

from objects import player_x, player_y, player_z


labels = {}

tp = Teleport(
    player_x,
    player_y,
    player_z,
    labels,
)
