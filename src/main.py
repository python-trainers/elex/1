from os import _exit as force_exit

from trainerbase.codeinjection import safely_eject_all_code_injections

from gui import run_menu
from scripts import regeneration_script_engine, system_script_engine


def on_initialized():
    system_script_engine.start()
    regeneration_script_engine.start()


def on_shutdown():
    system_script_engine.stop()
    regeneration_script_engine.stop()

    safely_eject_all_code_injections()


def main():
    run_menu(on_initialized=on_initialized)
    on_shutdown()
    force_exit(0)


if __name__ == "__main__":
    main()
