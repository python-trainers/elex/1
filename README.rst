====
Elex
====

Steam, build 2617898

How To Run
==========

Prepare your environment (once)
-------------------------------

1. Install `Python <https://www.python.org/downloads/>`__ 3.12

2. Install `Poetry <https://python-poetry.org/>`__

.. code:: bash

    python -m pip install --user pipx
    pipx install poetry

3. Install dependencies

.. code:: bash

    poetry install

Run
---

Using `Make <https://www.gnu.org/software/make/>`_
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    make

If `Make`_ is not installed
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code::

    poetry run python src/main.py
